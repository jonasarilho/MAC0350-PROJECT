--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao)
VALUES
( 'MAC0101', 4, 0, 60, '2015-01-01' ),
( 'MAC0110', 4, 0, 60, '1998-01-01' ),
( 'MAC0121', 4, 0, 60, '2015-01-01' ),
( 'MAC0350', 4, 0, 60, '2017-06-01' ),
( 'MAC0426', 4, 0, 60, '1998-01-01' ),
( 'MAC0444', 4, 0, 60, '1998-01-01' ),
( 'MAC0218', 4, 0, 60, '1998-01-01' ),
( 'MAC0328', 4, 0, 60, '1998-01-01' );

INSERT INTO
public.grade_obrigatoria (
  id_disciplina,
  ano_grade_obrigatoria)
VALUES
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0101' LIMIT 1), '01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1) ,'01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1) ,'01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1) ,'01-01-2015' );

INSERT INTO
public.grade_optativa (
  id_disciplina,
  ano_grade_optativa)
VALUES
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1), '01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0444' LIMIT 1) ,'01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0218' LIMIT 1) ,'01-01-2015' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0328' LIMIT 1) ,'01-01-2015' );


--------------------------------------------------------------------------------
--- Insere Trilhas
--------------------------------------------------------------------------------
INSERT INTO
public.trilhas(
  id_trilha,
  codigo_trilha,
  nome)
VALUES
( '1', 'T01', 'Trilha 1'),
( '2', 'T02', 'Trilha 2'),
( '3', 'T03', 'Trilha 3');

--------------------------------------------------------------------------------
--- Insere Modulos
--------------------------------------------------------------------------------
INSERT INTO
public.modulos (
  id_modulo,
  codigo_modulo,
  id_trilha)
VALUES
( '1', 'M01', 1),
( '2', 'M02', 1),
( '3', 'M03', 2),
( '4', 'M04', 3);

INSERT INTO
public.optativas_compoem_modulos (
  id_disciplina,
  ano_grade_optativa,
  id_modulo
)
VALUES
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0218' LIMIT 1), '01-01-2015', '1' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0328' LIMIT 1), '01-01-2015', '2' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1), '01-01-2015', '3' ),
((SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0444' LIMIT 1), '01-01-2015', '4' );

--------------------------------------------------------------------------------
--- Insere pessoas em seus respectivos cargos
--------------------------------------------------------------------------------

INSERT INTO
public.pessoas(nome)
VALUES
( 'Jef'    ),
( 'Décio'  ),
( 'Renato' );

INSERT INTO
public.administradores(id_pessoa, nusp_administrador, data_inicio)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  '2018-01-01'
);

INSERT INTO
public.professores(id_pessoa, nusp_professor, departamento)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  'Departamento de Ciência da Computação'
);

INSERT INTO
public.alunos(id_pessoa, nusp_aluno, turma_ingresso)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  '0000002',
  '2015-03-01'
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  '0000003',
  '2012-03-01'
);

--------------------------------------------------------------------------------
--- Cria estrutura de autenticação
--------------------------------------------------------------------------------

INSERT INTO
auth.usuarios(email_usuario, senha, expira)
VALUES
( 'jef@ime.usp.br', '12345', '2019-01-01' ),
( 'decio@ime.usp.br', '12345', '2019-01-01' ),
( 'renatocf@ime.usp.br', '12345', '2019-01-01' );

INSERT INTO
public.pessoas_geram_usuarios(id_pessoa, id_usuario)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br')
);

INSERT INTO
auth.perfis(nome_perfil)
VALUES
( 'administrador' ), -- 1
( 'professor' ), -- 2
( 'aluno' ); -- 3

INSERT INTO
auth.servicos(caminho_servico)
VALUES
( '/'             ), -- 1
( '/index'        ), -- 2
( '/estrutura'    ), -- 3
( '/plano'        ), -- 4
( '/pessoas'      ), -- 5
( '/pessoas/index'), -- 6
( '/pessoas/new'  ), -- 7
( '/pessoas/edit' ), -- 8
( '/pessoas/show' ); -- 9
( '/administradores'      ), -- 10
( '/administradores/index'), -- 11
( '/administradores/new'  ), -- 12
( '/administradores/edit' ), -- 13
( '/administradores/show' ); -- 14
( '/usuarios'      ), -- 15
( '/usuarios/index'), -- 16
( '/usuarios/new'  ), -- 17
( '/usuarios/edit' ), -- 18
( '/usuarios/show' ); -- 19
( '/administradores_coordenam_grades'      ), -- 20
( '/administradores_coordenam_grades/index'), -- 21
( '/administradores_coordenam_grades/new'  ), -- 22
( '/administradores_coordenam_grades/edit' ), -- 23
( '/administradores_coordenam_grades/show' ); -- 24

INSERT INTO
auth.usuarios_possuem_perfis(id_usuario, id_perfil, expira)
VALUES
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
);

INSERT INTO
auth.perfis_acessam_servicos(id_perfil, id_servico, expira)
VALUES
-- Administrador
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/pessoas/show'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores/show'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/usuarios'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/usuarios/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/usuarios/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/usuarios/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/usuarios/show'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores_coordenam_grades'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores_coordenam_grades/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores_coordenam_grades/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores_coordenam_grades/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/administradores_coordenam_grades/show'),
  '2019-01-01'
),
-- Professor
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
-- Estudante
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
);
