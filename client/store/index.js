import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
})

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      token: '',
      email: '',
      info: '',
      disc_feitas: 0,
      disc_fazendo: 0,
    },

    getters: {
      isAuthenticated(state) {
        return state.token !== '';
      },
      getInfo(state) {
        return state.info;
      },
      getEmail(state) {
        return state.email;
      },
      getDiscFeitas(state) {
        return state.disc_feitas;
      },
      getDiscFazendo(state) {
        return state.disc_fazendo;
      },
      getToken(state) {
        return state.token;
      }
    },

    mutations: {
      setToken(state, { token }) {
        state.token = token;
      },
      setDiscInfo(state, { info }) {
        state.info = info;
      },
      setEmail(state, { email }) {
        state.email = email;
      },
      resetState(state) {
        state.token = '',
        state.email = '',
        state.disc_feitas = 0,
        state.disc_fazendo = 0
      },
      incrementDiscFeitas(state) {
        state.disc_feitas = state.disc_feitas + 1;
      },
      incrementDiscFazendo(state) {
        state.disc_fazendo = state.disc_fazendo + 1;
      },
      decrementDiscFeitas(state) {
        state.disc_feitas = state.disc_feitas - 1;
      },
      decrementDiscFazendo(state) {
        state.disc_fazendo = state.disc_fazendo - 1;
      }
    }
  });
}
