# MAC0350-PROJECT

9793742 Enzo Hideki Nakamura<br>
9344935 Jonas Arilho Levy<br>
9778856 Ricardo Akira Tanaka<br>

Template for the project developed during the course MAC0350
(Introduction to Systems Development) at IME-USP.

This repository is a monorepo and requires [docker][1] and
[docker-compose][2] to run the services.

In order to setup the back-end services, open a shell and run:
```bash
cd server
docker-compose up
```

The Swagger UI is accessible through:<br>
[http://localhost:8080][3]

The PostgreSQL command line - psql - is reachable from the command:
```bash
docker-compose exec -e "LINES=$LINES" -e "COLUMNS=$COLUMNS" db psql -U dbz
```

In order to setup the front-end services, open another shell and run:
```bash
cd client
docker-compose build
docker-compose up
```

The client web page is reached from:<br>
[http://localhost:5000][4]

System confirmed working on Google Chrome Version 67.0.3396.99 (Official Build) (64-bit)

[1]: https://store.docker.com/search?type=edition&offering=community
[2]: https://docs.docker.com/compose/install/
[3]: http://localhost:8080
[4]: http://localhost:5000
